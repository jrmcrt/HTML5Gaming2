class Bonus {

	constructor({name, activable, action}) {
		this.name = name;
		this.activable = activable;
		this.action = action;
	}

	do(game, ship, stage) {
		if(!this.activable(ship))
			return;

		this.action.bind(game, ship, stage).call();
	}

}
