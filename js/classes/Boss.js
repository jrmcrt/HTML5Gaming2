class Boss extends Enemy {

	constructor(
		{game, stage, image, speed, lives, points, damagesImg, fireImg, pattern, shoot}
	) {
		super(...arguments);
		this.dropBonus = false;
		this.onDeathAction = null;
	}

	onDeath() {
		super.onDeath();
		this.game.nextLevel();
	}

	onBump() { }
}
