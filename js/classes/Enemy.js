class Enemy extends Entity {

	constructor(
		{game, stage, image, speed, lives, points, damagesImg, fireImg, pattern, shoot}
	){
		super(...arguments);
		this.fireImg = fireImg;
		this.shoot = shoot;
		this.dropBonus = true;
		this.onDeathAction = () => {
			this.game.kills++;
		}
	}

	onDeath() {
		super.onDeath();
	}

}
