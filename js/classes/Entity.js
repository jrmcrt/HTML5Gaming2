class Entity {

	constructor({game, stage, image, speed, lives, points, damagesImg, pattern}) {
		this.game = game;
		this.stage = stage;
		this.image = image;
		this.speed = speed;
		this._lives = lives;
		this.points = points;
		this.pattern = pattern;
		this.damagesImg = damagesImg;
		this.dropBonus = false;
		this.moving = false;
		this.added = false;
		this.alive = true;
		this.collision = null;
		this.onDeathAction = null;
		this.bitmap = null;
	}

	set lives(value) {
		this._lives = value;
		if (value <= 0) {
			this.onDeath();
		}
	}

	get lives() {
		return this._lives;
	}

	setDamageImage() {
		const damageImg = this.damagesImg[Math.floor(this.lives)];
		if (damageImg) {
			this.bitmap.image.src = 'img/' + damageImg;
		}
	}

	onHit(firepower, collision) {
		this.lives -= firepower;
		this.collision = collision;
		this.setDamageImage();
	}

	onDeath() {
		this.alive = false;
		this.game.score.points += this.points;
		if (this.dropBonus && rand(0, 10) > 8) { // TODO: put in conf
			this.game.dropBonus(this.collision);
			// Bonus drop pos seems a bit off
		}
		if (this.onDeathAction) {
			this.onDeathAction();
		}
		this.remove();
	}

	kill() {
		this.lives = 0;
	}

	remove() {
		this.stage.removeChild(this.bitmap);
		this.game.enemies.splice(this.game.enemies.indexOf(this), 1);
	}

	onBump() {
		// bumped by player ship
		this.kill();
	}

}
