function init() {

	var stage = new createjs.Stage("canvas");

	var game = {
		level: 1,
		shots: [],
		enemiesShots: [],
		kills: 0,
		toKill: 0,
		enemies: [],
		bonuses: [],
		possibleBonuses: Object.keys(imgs.bonus),
		score: {
			_points: 0,
			set points(value) {
				const gainedPoints = value - this._points;
				this._points = value;
				game.addScore(gainedPoints);
			},
			get points() {
				return this._points;
			},
			bitmap: false
		},
		text: {
			score: false,
			gameOver: false,
			start: false,
			level: false,
			pause: false
		},
		livesBitmap: [],
		state: {
			pause: false,
			over: false,
			started: false,
			moving: false,
			boss: false
		},

		setupManifest: function(images){
		    for(let i in images)
				typeof images[i] === 'string' ? this.manifest.push({src: 'img/' + images[i]}) : this.setupManifest(images[i]);
		},

		_preload: function(){
			this.manifest = [];
			this.setupManifest(imgs);
			let preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);
			preload.on("fileload", this.handleFileLoad);
			preload.on("complete", this.start.bind(this));
			preload.on("error", this.loadError);
			preload.loadManifest(this.manifest);
		},

		handleFileLoad: function(event){
			console.log(`File loaded -> ${event.item.src}`);
		},

		loadError: function(event){
			console.error(`[HMLT5Gaming2] Could not load -> ${event.item.src}`);
		},

		startScreen: function(){
			stage.enableMouseOver(10);
			this.text.start = new createjs.Text("START", "50px Future", "#FFFFFF");
		    this.text.start.x = stage.canvas.width/2 - this.text.start.getMeasuredWidth()/2;
		    this.text.start.y = stage.canvas.height/2 - this.text.start.getMeasuredHeight()/2;
		    var hit = new createjs.Shape();
			hit.graphics.beginFill("#000")
				.drawRect(0, 0, this.text.start.getMeasuredWidth(), this.text.start.getMeasuredHeight());
			this.text.start.hitArea = hit;
			this.text.start.cursor = 'Pointer';
			this.text.start.alpha = 0.7;
			this.text.start.on("mouseover", function(event) { game.text.start.alpha = 1; });
			this.text.start.on("mouseout", function(event) { game.text.start.alpha = 0.7; });
			this.text.start.addEventListener("click", function(event) {
				game.state.started = true;
				game.start();
			});
		    stage.addChild(this.text.start);
		    stage.update();
		},

		start: function(){
			if(!this.state.started)
			{
				stage.enableMouseOver(0);
				this.startScreen();
				document.onkeydown = handleKeyDown;
				document.onkeyup = handleKeyUp;
				createjs.Ticker.addEventListener("tick", handleTick);
				createjs.Ticker.setFPS(60);
				sounds.register();
			} else { //triggered when user clicks start
				ship.append();
				this.toKill = levels[this.level].enemies.length;
				stage.removeChild(this.text.start);
				this.addScore();
				this.handleLives();
				this.levelAnim();
				this.addMeteor();
			}
		},

		addEnemy: function(enemy, meteor = false){
			enemy.bitmap = new createjs.Bitmap('img/' + enemy.image);
			stage.addChild(enemy.bitmap);
			var EnemyY = !meteor ? rand(5, 100) : - enemy.bitmap.image.height;
			var EnemyX = rand(5, stage.canvas.width - enemy.bitmap.image.width);
			enemy.bitmap.x = rand(5, stage.canvas.width - enemy.bitmap.image.width); 
			enemy.bitmap.y = - enemy.bitmap.image.height ;
			enemy.added = false;
			var dist = distance(enemy.bitmap.x, enemy.bitmap.y, EnemyX, EnemyY);
			var speed = 141;//magic number :D
			var time = (dist / speed) * 1000;
			this.enemies.push(enemy);
			createjs.Tween.get(enemy.bitmap)
                .to({x: EnemyX, y: EnemyY}, time, createjs.Ease.getPowInOut(1))
                .call(function(){enemy.added = true;})
                .call(function(){enemy.pattern(game);})
		
		},

		addMeteor: function(){ //can actually also add a regular enemy
			if(this.state.over || this.state.boss)
				return false;

			if(levels[this.level].enemies.length && rand(0, 100) > 50 && !this.state.pause){ //add enemy odds to conf
				var index = rand(0, levels[this.level].enemies.length - 1);
				const enemy = levels[this.level].enemies[index];
				this.addEnemy(new Enemy({
					game,
					stage,
					...enemy.stat,
					pattern: enemy.pattern,
					shoot: enemy.shoot
				}), false);
				levels[this.level].enemies.splice(index, 1);
			}
			// Adding a meteor
			if(rand(0, 100) > 75 && !this.state.pause){ //add enemy odds to conf
				var temp = new Entity({
					game,
					stage,
					...enemies.meteor.stat,
					pattern: enemies.meteor.pattern,
				});
				if(this.level > 1)
					temp.image = imgs.rocks.med[rand(0, 1)];
				this.addEnemy(temp, true);
			}
			
			if(!this.state.boss){ //why stop ?
				setTimeout(function(){
					game.addMeteor();
				}, 1000);
			}
		},

		nextLevel: function(){
			if(this.level + 1 <= Object.keys(levels).length){ //empty enemy and shot arrays here
				this.level++;
				this.levelAnim();
				this.state.boss = false;
				this.toKill = levels[this.level].enemies.length;
				this.kills = 0;
				this.addMeteor();
			}
			else
				this.gameOver(true);
		},

		dropBonus({x, y}) {
			var bonuses = Object.keys(imgs.bonus);
			var randBonus = bonuses[rand(0, bonuses.length - 1)];
			var bonus = new createjs.Bitmap('img/' + imgs.bonus[randBonus]);
			bonus.type = randBonus;
			bonus.x = x;
			bonus.y = y;
			this.bonuses.push(bonus);
			stage.addChild(bonus);
			createjs.Tween.get(bonus)
				.to({y: y + stage.canvas.height},
					3000, createjs.Ease.getPowInOut(1));
		},

		handleCollisions: function(){
			//shots hit enemies
			for(var i = this.shots.length - 1; i >= 0; i--){
				for(var j = this.enemies.length - 1; j >= 0; j--){
					var collision = ndgmr.checkPixelCollision(this.shots[i], this.enemies[j].bitmap, 0);
					if(collision && this.enemies[j].bitmap.y > - this.enemies[j].bitmap.image.height
						&& this.shots[i].y > - this.shots[i].image.height) //why the bitmap position check tho ?
					{
						this.createImpact(collision);
						stage.removeChild(this.shots[i]);
						this.shots.splice(i, 1);
						this.enemies[j].onHit(ship.firePower, collision);
						break; // one shot can not hit more than one enemy
					}
				}	
			}
			//collision enemy/ship
			for(var j = this.enemies.length - 1; j >= 0 && !this.state.over; j--){
				if(this.enemies[j].bitmap !== undefined)
				{
					var collisionShip = ndgmr.checkPixelCollision(ship.bitmap, this.enemies[j].bitmap, 0);
					if(collisionShip && !ship.invincible && !this.state.over )
					{
						this.createImpact(collisionShip);
						var sound = createjs.Sound.play('lose');
						sound.volume = 1;
						ship.lives--;
						this.enemies[j].onBump();
						ship.invincible = true;
					}
				}
			}
			//collision enemies shots/ship
			for(var i = this.enemiesShots.length - 1; i >= 0 && !this.state.over ; i--)
			{
				var collisionShot = ndgmr.checkPixelCollision(ship.bitmap, this.enemiesShots[i], 0);
				if(collisionShot && !ship.invincible && !this.state.over && !ship.shield)
				{
					this.createImpact(collisionShot);
					stage.removeChild(this.enemiesShots[i]);
					this.enemiesShots.splice(i, 1);
					var sound = createjs.Sound.play('lose');
					sound.volume = 1;
					ship.lives--;
					ship.invincible = true; //function shipTakeshits
				}
				if(ship.shield){
					var collisionShield = ndgmr.checkPixelCollision(ship.shield, this.enemiesShots[i], 0);
					if(collisionShield){
						this.createImpact(collisionShield);
						stage.removeChild(this.enemiesShots[i]);
						this.enemiesShots.splice(i, 1);	
					}
				}
			}
			// collision ship / bonus
			for(var i = this.bonuses.length - 1; i >= 0 && !this.state.over; i--)
			{
				var collisionBonus = ndgmr.checkPixelCollision(ship.bitmap, this.bonuses[i], 0);
				if(collisionBonus && !game.state.over)
				{
					stage.removeChild(this.bonuses[i]);
					this.handleBonus(this.bonuses[i].type);
					stage.removeChild(this.bonuses[i]);
					this.bonuses.splice(i, 1);
					var sound = createjs.Sound.play('bonus');
					sound.volume = 2;
				}
			}	
			
		},

		createImpact: function(collision){
			var impact = new createjs.Bitmap('img/' + imgs.fire.hit.small);
			stage.addChild(impact);
			impact.x = collision.x - (impact.image.width / 2);
			impact.y = collision.y - (impact.image.height / 2);
			setTimeout(function(){stage.removeChild(impact);}, 50);
		},

		handleLives: function(){
			if(!this.livesBitmap.length)
				for(var i = 0; i < ship.lives; i++){
					var temp = new createjs.Bitmap(ship.getLifeImage());
					temp.x = 20;
				    temp.y = 20 + (i * 40);
				    this.livesBitmap.push(temp);
				    stage.addChild(temp);
				}

			for(var i = 0; i < this.livesBitmap.length; i++)
				if(ship.lives < i + 1)
				{
					stage.removeChild(this.livesBitmap[i]);
					this.livesBitmap.splice(i, 1);
					break;
				}

			if(ship.lives > this.livesBitmap.length)
			{
				var temp = new createjs.Bitmap(ship.getLifeImage());
				temp.x = 20;
			    temp.y = 20 + (this.livesBitmap.length * 40);
			    this.livesBitmap.push(temp);
			    stage.addChild(temp);
			}		    
		},

		handleBonus: function(bonusName){
			const bonus = bonuses.find(b => b.name === bonusName);
			bonus.do(game, ship, stage);
		},

		addScore: function(toAdd = false){
			if(!this.score.bitmap)
			{
				this.score.bitmap = new createjs.Text('00000', "50px future", "#FFFFFF");			    
				this.score.bitmap.x = stage.canvas.width - 210;
			    this.score.bitmap.y =  20;
			    stage.addChild(this.score.bitmap);
			}
			this.score.bitmap.text = '0'.repeat(5 - String(this.score.points).length) + this.score.points;
			if(toAdd)
			{
				var toAdd = new createjs.Text( '+ ' + toAdd,
				 "20px future", "#FFFFFF"); //why not increase size proportionally to score and randomize position a little ?
				toAdd.x = stage.canvas.width - 110;
				toAdd.y = 80;
				toAdd.alpha = 1;
				stage.addChild(toAdd);
				createjs.Tween.get(toAdd)
                	.wait(500)
                	.to({alpha: 0}, 500, createjs.Ease.getPowInOut(1));
			}
		},

		levelAnim: function(){
			this.text.level = new createjs.Text("LEVEL " + String(this.level), "50px Future", "#FFFFFF");
			this.text.level.x = this.text.start.x = stage.canvas.width/2 - this.text.level.getMeasuredWidth()/2;
			this.text.level.y = stage.canvas.height/2 - this.text.level.getMeasuredHeight()/2;
			this.text.level.alpha = 0;
			stage.addChild(this.text.level);
			createjs.Tween.get(this.text.level)
                .to({alpha: 1}, 600, createjs.Ease.getPowInOut(1))
               	.wait(800)
               	.to({alpha: 0}, 600, createjs.Ease.getPowInOut(1));
		},

		gameOver: function(win = false){
			if(!this.text.gameOver)
			{
				this.text.gameOver = new createjs.Text('GAME OVER', "70px future", "#FFFFFF");
				if(win) {
					this.text.gameOver.text = 'YOU WIN !';
					var lifescore = new createjs.Text('+ ' + String(this.livesBitmap.length * 1000), "40px future", "#FFFFFF");
					stage.addChild(lifescore);
					lifescore.x = stage.canvas.width / 2 - lifescore.getMeasuredWidth() / 2;
					lifescore.y = stage.canvas.height - 280;
					createjs.Tween.get(lifescore)
                	.wait(2000)
                	.to({alpha: 0}, 500, createjs.Ease.getPowInOut(1));
					this.score.points += this.livesBitmap.length * 1000;
					this.score.bitmap.text = String(this.score.points);
				}
				this.text.gameOver.x = stage.canvas.width / 2 - this.text.gameOver.getMeasuredWidth() / 2;
				this.text.gameOver.y =  250;
				stage.addChild(this.text.gameOver);


				this.text.score = new createjs.Text(this.score.bitmap.text + " POINTS", "55px future", "#FFFFFF");			    
				this.text.score.x = stage.canvas.width / 2 - this.text.score.getMeasuredWidth() / 2;
			    this.text.score.y =  350;
			    stage.addChild(this.text.score);

			    stage.enableMouseOver(10);
				var replay = new createjs.Text("REPLAY", "40px Future", "#FFFFFF");
		    	replay.x = stage.canvas.width/2 - replay.getMeasuredWidth()/2;
		    	replay.y = stage.canvas.height - 200;
		    	replay.cursor = 'Pointer';
		    	var hit = new createjs.Shape();
				hit.graphics.beginFill("#000").drawRect(0, 0, replay.getMeasuredWidth(), replay.getMeasuredHeight());
				replay.hitArea = hit;
				replay.alpha = 0.7;
				replay.on("mouseover", function(event) { replay.alpha = 1; });
				replay.on("mouseout", function(event) { replay.alpha = 0.7; });
				replay.addEventListener("click", function(event) { location.reload(); })
		    	stage.addChild(replay);

			    stage.removeChild(this.score.bitmap);
			    stage.removeChild(ship.shipContainer);
			    ship.shield = false;
			    stage.removeChild(ship.shield);
			    ship.speedBitmap = false;
			    stage.removeChild(ship.speedBitmap);
			    this.state.over = true;
			}
		}

	};

	var ship = {
		bitmap: false,
		speed: 6,
		speedBitmap: false,
		shield: false, 
		image: imgs.ship,
		_lives: 5,
		set lives(value) {
			this._lives = value;
			game.handleLives();
			if (value === 0) {
				game.gameOver();
			} else {
				let damageImg = imgs.ship.damages.types[this.type][Math.floor(value)] !== undefined
					? 'img/' + imgs.ship.damages.types[this.type][Math.floor(value)] : false;
				if (damageImg) {
					if(!this.shipContainer.contains(this.damageImg)) {
						this.damageImg = new createjs.Bitmap(damageImg);
						this.damageImg.x = ship.bitmap.x;
						this.damageImg.y = ship.bitmap.y;
						this.shipContainer.addChild(this.damageImg);
					} else
						this.damageImg.image.src = damageImg;
				}
			}
		},
		get lives() {
			return this._lives;
		},
		_invincible : false,
		set invincible(value) {
			this._invincible = value;
			this.shipContainer.alpha = value ? 0.5 : 1;
			if(value)
				setTimeout(function(){
					ship._invincible = false;
					ship.shipContainer.alpha = 1;
				}, 1000);
		},
		get invincible() {
			return this._invincible;
		},
		canFire: true,
		firing: false,
		firePower: 1,
		direction: {left: false, right: false, up: false, down: false},
		colour: false,
		type: false,

		append: function(){
			this.colour = misc.ship.possibleColours[rand(0, misc.ship.possibleColours.length - 1)];
			this.type = misc.ship.types[rand(0, misc.ship.types.length - 1)];
			this.image = this.getImage();
			this.bitmap = new createjs.Bitmap(this.image);
			this.shipContainer = new createjs.Container();
			this.shipContainer.addChild(this.bitmap);
			stage.addChild(this.shipContainer);
			this.shipContainer.x = (stage.canvas.width / 2) - (this.bitmap.image.width / 2);
			this.shipContainer.y = stage.canvas.height - 100;
		}, 

		getImage: function(){
			return `img/spaceshooter/PNG/playerShip${this.type}_${this.colour}.png`;
		},

		getLifeImage: function(){
			return `img/spaceshooter/PNG/UI/playerLife${this.type}_${this.colour}.png`;
		},

		shoot: function(){
			if(!game.state.started || game.state.pause || game.state.over) //create fonction inPlay ?
				return false;
			if(this.canFire)
			{	
				var fire = new createjs.Bitmap('img/' + imgs.fire.player[Math.ceil(this.firePower)]);
				stage.addChild(fire);
				fire.y = this.shipContainer.y - 35;
				fire.x = this.shipContainer.x + this.bitmap.image.width / 2 - (fire.image.width / 2);
				fire.id = uniqid();
				game.shots.push(fire)
				createjs.Tween.get(fire)
					.to({y: this.bitmap.y - stage.canvas.height}, 1250, createjs.Ease.getPowInOut(1))
					.call(function(){
						//removing lost shot from stage and shots array, should create a function for this later
						stage.removeChild(fire);
						let fireIndex = game.shots.findIndex(f => f.id === fire.id);
						if(fireIndex !== -1)
							game.shots.splice(fireIndex, 1);
					});
				var sound = createjs.Sound.play('fire');
				sound.volume = 0.3;
				this.canFire = false;
				setTimeout(function(){ship.canFire = true}, 250);   
			}
		},

		move: function(dir){ //the ships position is off at the start for some reason
			if(!game.state.started || game.state.over || game.state.pause)
				return false;

			var axis = false;
			var vector = false;
			if(dir === 'left' && this.shipContainer.x > 5){//still room for improvements
				axis = 'x';
				vector = -this.speed;
			}
			else if(dir === 'right' && this.shipContainer.x < (stage.canvas.width - this.bitmap.image.width) - 5){
				axis = 'x';
				vector = this.speed;
			}
			else if(dir === 'up' && this.shipContainer.y > 5){
				axis = 'y';
				vector = -this.speed;
			}
			else if(dir === 'down' && this.shipContainer.y < (stage.canvas.height - this.bitmap.image.height) - 5){
				axis = 'y';
				vector = this.speed;
			}

			if(axis)
				this.shipContainer[axis] += vector;
		}

	};

	game._preload();

	function handleTick(event){
		for(var v in ship.direction)
			if(ship.direction[v])
				ship.move(v);

		if(ship.firing)
			ship.shoot();

		game.handleCollisions();

		if(levels[game.level].enemies.length === 0){ // check for bossPhase
			if(game.kills === game.toKill && !game.state.boss){ // why kill check ?
				game.state.boss = true;
				const boss = levels[game.level].boss;
				game.addEnemy(new Boss({
					game,
					stage,
					...boss.stat,
					pattern: boss.pattern,
					shoot: boss.shoot,
				}), false);
			}
		}

		stage.update()
	}

	function handleKeyDown(e){
		var key = e.keyCode;
		for(var v of Object.keys(keys.direction))
			if(key == keys.direction[v])
				ship.direction[v] = true;

		if(keys.fire.includes(key))
			ship.firing = true;

		if(keys.pause.includes(key) && game.state.started && !game.state.over)
		{
			if(!game.state.pause)
			{
				game.state.pause = true;
				createjs.Ticker.setPaused(paused = true);
                game.text.pause = new createjs.Text('PAUSE', '75px Future', '#FFFFFF');
                game.text.pause.x = 340;
                game.text.pause.y = 300;
                stage.addChild(game.text.pause);
                stage.alpha = 0.5;
			} else {
				createjs.Ticker.setPaused(paused = false);
                stage.removeChild(game.text.pause);
                stage.alpha = 1;
				game.state.pause = false;
			}
		}
	}

	function handleKeyUp(e){
		var key = e.keyCode;
		for(var v of Object.keys(keys.direction))
			if(key == keys.direction[v])
				ship.direction[v] = false;

		if(keys.fire.includes(key))
			ship.firing = false;	
	}

}
